﻿using PK.Container;
using System;
using System.Reflection;

namespace Lab6.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            var container = LabDescriptor.ContainerFactory();
            container.Register(new Lab6.Display.Implementation.Display());
            container.Register(LabDescriptor.ControlPanelImpl);
            container.Register(LabDescriptor.MainComponentImpl);

            return container;
        }
    }
}
